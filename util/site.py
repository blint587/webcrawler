# encoding: utf-8


class WebSite:

    def __init__(self, url, status_code=None, page_size=None):
        self.__url = url
        self.__staus_code = status_code
        self.__page_size = page_size

    def __repr__(self):
        return "URL: {url} - Status Code: {stat_code} - Size: {size}".format(url=self.__url, stat_code=self.__staus_code, size=self.__page_size)

    def __str__(self):
        return "URL: {url} - Status Code: {stat_code} - Size: {size}".format(url=self.__url, stat_code=self.__staus_code, size=self.__page_size)

    def __hash__(self):
        return hash(self.__url)


    @property
    def url(self):
        return self.__url

    @property
    def status_code(self):
        return self.__staus_code

    @status_code.setter
    def status_code(self, v):
        self.__staus_code = v

    @property
    def page_size(self):
        return self.__page_size


    @page_size.setter
    def page_size(self, v):
        self.__page_size = v