# encoding: utf-8
import logging
from HTMLParser import HTMLParser
from Queue import Empty
from Queue import Queue as Q
from httplib import InvalidURL

import requests
from requests.exceptions import InvalidURL, ConnectionError

from util.site import WebSite
from util.site_storage import SiteStorage, URLStorage

logger = logging.getLogger()


# https://stackoverflow.com/questions/3075550/how-can-i-get-href-links-from-html-using-python
class LinkParser(HTMLParser):
    """
    HTML parser that looks for anchor tags and returns the links in them.
    """

    def __init__(self):
        HTMLParser.__init__(self)
        self._links = []

    def reset(self):
        HTMLParser.reset(self)
        self._links = []

    def handle_starttag(self, tag, attrs):
        if "a" == tag:
            for name, value in attrs:
                if "href" == name:
                    self._links.append(value)

    def get_links(self):
        return self._links


class Consumer:

    def __init__(self, domain, q, site_info, url_storage):
        """
        :type domain: str
        :type q: Q
        :type site_info: SiteStorage
        :type url_storage: URLStorage
        """
        self.__domain = domain
        self.__q = q
        self.__site_info = site_info
        self.__url_storage = url_storage

    def __call__(self, time_out=None):
        """

        :param time_out: queue timeout parameter
        :return:
        """
        exit_flag = True
        while exit_flag:
            try:
                page = unicode(self.__q.get(timeout=time_out))
                logger.info("Processing {}".format(page))

                if page not in self.__site_info:
                    if self.__domain not in page:
                        page = self.__domain + page
                    try:
                        page_info = requests.get(page)
                    except ConnectionError:
                        continue
                    except InvalidURL:
                        continue
                else:
                    continue

                new_site = WebSite(page, page_size=len(page_info.text), status_code=page_info.status_code)
                if self.__site_info.add(new_site):
                    sites = self._get_links(page_info.text)
                else:
                    continue
                for site in sites:
                    if site not in self.__url_storage:
                        self.__url_storage.add(site)
                        self.__q.put(site)
                        logger.info("New link found: {}".format(site))
            except Empty:
                logger.info("Timeout elapsed ({} s), consumer exits.".format(time_out))
                exit_flag = False

    def _in_domain(self, link):
        """
        Checks if the link is in domain.
        :param link: url to be checked
        :return: if the link is in domain
        """
        return (("http" not in link) and ("https" not in link)) or (self.__domain in link)

    def _valid_link(self, link):
        """
        Filters out id references and pictures
        :param link: url to be checked
        :return: if the link is valid
        """
        # the list needs to be expanded
        return all([st not in link for st in ["#", "jpg", "jpeg", "png", "gif", "tar", "exe",
                                              "msi", "py", "@", "ftp", "pdf", "bat", "txt"]])

    def _get_links(self, site):
        """
        Generator object which returns valid and in-domain links.
        :param site:
        :return:
        """
        lp = LinkParser()

        lp.feed(site)
        for link in lp.get_links():
            if self._in_domain(link) and self._valid_link(link):
                yield link





