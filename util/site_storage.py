# encoding: utf-8
from threading import RLock
from util.site import WebSite


# Storing actual queried and un-queried sites separably is not memory efficient, but this made
# the architecture a bit easier.
# For bigger scale I would use some database backed solution with an ORM layer (sqlalchemy)
class SiteStorage:

    def __init__(self):
        # Using a set so no duplicates appear, and also search in Python set implementation is O(1)
        self.__storage = set()
        self.__lock = RLock()

    def add(self, site):
        """
        Adds a site to the storage.
        :type site: WebSite
        :return: bool
        """
        if isinstance(site, WebSite):
            self.__lock.acquire()
            try:
                if site not in self.__storage:
                    self.__storage.add(site)
                    return True
                else:
                    return False
            finally:
                self.__lock.release()

    def __contains__(self, item):
        return item in self.__storage

    def __iter__(self):
        return self.__storage.__iter__()


class URLStorage:

    def __init__(self):
        self.__storage = set()
        self.__lock = RLock()

    def add(self, url):
        """
        Adds a url to the storage.
        :param url:
        :return:
        """
        self.__lock.acquire()
        self.__storage.add(url)
        self.__lock.release()

    def __contains__(self, item):
        self.__lock.acquire()
        try:
            return item in self.__storage
        finally:
            self.__lock.release()