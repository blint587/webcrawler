import os
import xml.etree.ElementTree as ET


class CrawlerConfig:
    def __init__(self):

        ippath = os.path.dirname(__file__)
        paths = ippath.split(';')
        # print paths

        for path in paths:
            try:
                with open(os.path.join(str(path), "config.xml")) as myfile:
                    data = "".join(line.rstrip() for line in myfile)
                    myfile.close()

                root = ET.fromstring(data)
                self.__nr_of_threads = int(root.find('crawler/nr_thread[@value]').attrib['value'])
                self.__timeout = int(root.find('crawler/timeout[@value]').attrib['value'])
            except IOError as ex_1:
                continue

    @property
    def nr_of_threads(self):
        return self.__nr_of_threads

    @property
    def timeout(self):
        return self.__timeout


config = CrawlerConfig()