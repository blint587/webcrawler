# encoding: utf-8
"""
Usage:
First commandline parameter needs to be the (valid) URL. (with the http:// prefix)
Second argument is optional, which is the number of parallel consumer threads to be started.
If it is not provided it will use the number in the config.xml.
Thread timeout can also be set in the config.xml (in second).

"""
import logging
import sys
from Queue import Queue as Q
from threading import Thread

from util.site_consumer import Consumer
from util.site_storage import SiteStorage, URLStorage
from crawlerconfig import config

if __name__ == "__main__":
    # In Python threads are high level threads and cannot utilize multiple cores.
    # For bigger scale application I would use multiprocessing instead of threading. In the this given application
    # I choose to use threading because concurrent access of storage can be controlled via thread locks, while
    # in multiprocessing application this problem is not so trivial due to "memory  copy" of the parent process.
    # In a multiprocessing approach a database backed storage would be a more straight forward solution.
    # In a much bigger scale I would use a Queue server based distributed system like RabbitMQ, to
    # distribute the work to multiple machines. This would require a centralized storage like a database.

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    stream_handler = logging.StreamHandler()
    logger.addHandler(stream_handler)

    site_storage = SiteStorage()
    url_storage = URLStorage()
    queue = Q()

    if len(sys.argv) > 1:
        url = sys.argv[1]
    else:
        logger.fatal("No URL has been provided! The first argument needs to be the URL.")
        sys.exit(1)

    if len(sys.argv) > 2:
        nr_of_threads = max(int(sys.argv[2]), 1)
    else:
        nr_of_threads = config.nr_of_threads

    logger.info("{} threads will be started!".format(nr_of_threads))

    queue.put(url)

    threads = list()
    for nr in range(nr_of_threads):
        c = Consumer(url, queue, site_storage, url_storage)
        threads.append(Thread(target=c, args=(config.timeout,)))

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    logger.info("Crawling finished! Printing results.")
    for site in site_storage:
        print(site)
