Your task is to implement a script solving the problem stated below. You should implement the task in Python 2.7 with standard libraries only. The exception is "requests" library (http://docs.python-requests.org/en/master/).
To master the programming exercise successfully you need to provide information about how you approached the problem. It is as important for us to understand how you thought about the given problem, as it is to see your actual solution.
If anything is unclear about this task, please make your own assumptions and also write them down, in order for us to be able to follow your train of thought.
Please hand in all your source code (with comments), additional descriptions and references that you have used. It is permitted to use all online sources available, but please state the references/resources used and why you have decided to do so.

Task

Write a concurrent website crawler.
Web crawler should travel across all links starting from the page specified by the user as command line parameter to the script. For every page fetched in the process it should display following information to standard output:
full link
status code
page size
It should not go out of domain boundaries. So if you specify http://abc.com/ then it should fetch pages under this domain of abc.com only. You should not save pages on the disk, only fetch and parse them.
User of the script should be able to limit the amount of concurrent threads ran by crawler via command line parameter.


Approach

The application will first create a storage (2) object which will be responsible for storing the information described in the task.
It is important to store URLs which have been scanned to avoid infinite scanning due to circular references on sites.
The specified number of consumers will be started. The consumers will get the URL via a thread safe Queue and query the site.
After site content has been fetched it will scan for anchor tags in the HTML text and retrieve the references stored in them.
Checking if the links are in domain, are not id links or images and has not been processed before.
Then each valid link is pushed into the same queue from which the original URL was fetched.
This continues until there is no more new links to consume, this is indicated by the timeout of the queue,
meaning no more links can be consumed from the queue.

The described approach is independent from the implementation. It can be applied to a smaller multi threaded application to
a much larger distributed system where the consumers are running on different machines, the queue is backed by a Queues server
and the centralized storage is a database.

In the given implementation the information needed is displayed from the storage after the crawling is finished.